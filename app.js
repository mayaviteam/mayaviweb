/**
 * Created by ddeodar on 4/3/2014.
 */
Ext.Loader.setConfig({
    enabled: true
});
Ext.Ajax.useDefaultXhrHeader = false;
Ext.Ajax.cors = true;


Ext.application({
    name: 'Mayavi',
    autoCreateViewport: true,
    models: ['User', 'Role'],
    stores: ['User','Role'],
    controllers: ["Main","User"]
});
