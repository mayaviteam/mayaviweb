/**
 * Created by ddeodar on 4/9/2014.
 */
Ext.define('Mayavi.view.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'mayavi_login',
    title: 'Login',
    frame: true,
    style:'margin:16% 36% 16% 36%',
    bodyPadding: 10,
    defaultType: 'textfield',
    defaults:{
        anchor:'100%'
    },
    items: [
        {
            allowBlank: false,
            fieldLabel: 'User ID',
            name: 'user',
            emptyText: 'user id'
        },
        {
            allowBlank: false,
            fieldLabel: 'Password',
            name: 'pass',
            emptyText: 'password',
            inputType: 'password'
        },
        {
            xtype:'checkbox',
            fieldLabel: 'Remember me',
            name: 'remember'
        }
    ],

    buttons :[{
        text:'Login',
        action:'doSubmitLogin'
    }]
});
