Ext.define('Mayavi.view.user.Home', {
	extend: 'Ext.window.Window',
	xtype: 'mayavi_user',
	title: 'User',
	layout: 'fit',
	maximized: true,
	model: true,
	closeAction: 'hide',
	items: {
		xtype: 'tabpanel',
		items: [
        {
    		title: 'Existing users',
            layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
			items: {
				xtype: 'mayavi_user_grid',
                flex: 1
			}

    	},{
			title: 'Add new user',
			items: {
				xtype: 'mayavi_user_form',
                padding: 10,
                border: false,
			}
		}]
	}
});
