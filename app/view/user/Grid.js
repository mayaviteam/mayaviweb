/*var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 1,
        autoCancel: false
});*/
Ext.define('Mayavi.view.user.Grid', {
	extend: 'Ext.grid.Panel',
	xtype: 'mayavi_user_grid',
    columns: [], // Added dynamically
	store: 'User',
    // Custom members
    _columns : [
                    { text: 'User name',  dataIndex: 'username' },
                    { text: 'First name', dataIndex: 'first_name'},
                    { text: 'Last name', dataIndex: 'last_name' }
                ],
    _action_column:  {
            xtype:'actioncolumn',
            width:50,
            items: [{
                icon: 'resources/images/edit.png',
                tooltip: 'Edit'
            },{
                icon: 'resources/images/delete.png',
                tooltip: 'Delete'
            }]
        }
});

