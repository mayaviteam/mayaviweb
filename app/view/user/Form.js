Ext.define('Mayavi.view.user.Form', {
	extend: 'Ext.form.Panel',
	xtype: 'mayavi_user_form',
    defaultType: 'textfield',
    items: [
        {
            fieldLabel: 'Username',
            name: 'username',
            allowBlank: false
        },{
            fieldLabel: 'First Name',
            name: 'first_name',
            allowBlank: false
        },{
            fieldLabel: 'Last Name',
            name: 'last_name',
            allowBlank: false
        }],
    buttons: [
        {
            text: 'Reset',
            handler: function() {
                this.up('form').getForm().reset();
            }
        },{
            text: 'Submit',
            formBind: true,
            action: 'doSaveUser'
        }]
});

