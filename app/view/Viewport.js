/**
 * Created by ddeodar on 4/11/2014.
 */
Ext.define('Mayavi.view.Viewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'mayavi_viewport',
    layout: 'card',
    config: {
        style: {
            backgroundImage: 'url(resources/images/elephants.jpg)',
            backgroundSize: '100% 100%',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'bottom left'
        },
        activeItem: 2,
        items:[
            {
                xtype:'mayavi_login'
            },
            {
                xtype:'mayavi_dashboard'
            },
            {
                xtype:'mayavi_user',
		autoShow: true
            }

	]
    }
});
