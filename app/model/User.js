Ext.define('Mayavi.model.User', {
	extend: 'Ext.data.Model',
	fields: ['id', 'username', 'password', 'first_name', 'last_name'],
    hasMany: { model: 'Mayavi.model.Role', name: 'roles', associationKey:'roles'},
	proxy: {
		type: 'rest',
		url : 'http://sparkitcs.dynu.com:3000/user',
        writer: {
            getRecordData : function (record, operation ){
                var data = record.data;
                if(record.raw.roles){
                    if(Ext.isArray(record.raw.roles)){
                        data['roles'] = record.raw.roles;
                    }else{
                        data['roles'] = [record.raw.roles];
                    }
                }
                return data;
            }
        },
        reader: {
            type: 'json'
        }
    }
});
