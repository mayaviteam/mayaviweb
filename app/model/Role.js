Ext.define('Mayavi.model.Role', {
	extend: 'Ext.data.Model',
	fields: ['id', 'name', 'desc'],
    hasMany: { model: 'Mayavi.model.User'},
	proxy: {
		type: 'rest',
		url : 'http://sparkitcs.dynu.com:3000/role',
        reader: {
            type: 'json'
        }
	}
});
