Ext.define('Mayavi.controller.Main',{
    extend: 'Ext.app.Controller',
    views: ['Login', 'Dashboard','user.Home'],
    refs: [
        {
            ref: 'login',
            selector: 'mayavi_login'
        },
        {
            ref: 'dashboard',
            selector: 'mayavi_dashboard'
        },
        {
            ref: 'viewport',
            selector: 'mayavi_viewport'
        },
        {
            ref: 'user',
            selector: 'mayavi_user'
        }

    ],
    init: function () {
        this.control({
            'button[action=doSubmitLogin]': {
                click: 'login'
            },
            'button[action=logout]': {
                click: 'logout'
            },
            'button[action=user]': {
                click: 'load_user'
            }
            /*
            ,
            'button[action=doSaveUser]': {
                click: 'save_user'
            }*/
        });
        /*var role_store = Ext.data.StoreManager.lookup('Role');
        role_store.on('load',function(store, records, successful, eOpts ){
            columns = [
                        { text: 'User name',  dataIndex: 'username' },
                        { text: 'First name', dataIndex: 'first_name'},
                        { text: 'Last name', dataIndex: 'last_name' }
                    ];

            var user_store = Ext.data.StoreManager.lookup('User');
            var role_checkboxes = [];
            var _userModelType = Mayavi.model.User;
            var _fields = _userModelType.getFields();
            for (var i = 0; i < records.length; i++) {
                // User form
                var checkbox = {
                    xtype: 'checkboxfield',
                    name: 'roles',
                    boxLabel: records[i].get('name'),
                    inputValue: {'id':records[i].get('id'),'name':records[i].get('name')}
                };
                role_checkboxes.push(checkbox);
                // User Grid
                columns.push({
                    text:records[i].get('name'),
                    dataIndex: records[i].get('name')
                });
                // User model
                _fields.push({
                    name:records[i].get('name'),
                    type:'boolean',
                    convert: function(v, record){
                        var ret = false;
                        //console.log(JSON.stringify(record.raw.roles));
                        if(record.raw.roles){
                            for(i=0;i<record.raw.roles.length;i++){
                                var role = record.raw.roles[i]
                                if(this.name == role.name){
                                    ret = true;
                                    break;
                                }
                            }
                        }
                        return ret;
                    }
                });
            }
            _userModelType.setFields(_fields);
            var obj = {
                xtype: 'fieldcontainer',
                fieldLabel: 'roles',
                defaultType: 'checkboxfield',
                items: role_checkboxes
            }
            this.getUser().query('.mayavi_user_form')[0].add(obj);

            user_store.load({
                scope: this,
                callback: function(){
                    alert("Here");
                    this.getUser().query('.gridpanel')[0].reconfigure(user_store,columns);
                }
            });
        }, this);
        */
    },
    login: function () {
        var formValues = this.getLogin().getValues();
        this.getViewport().getLayout().setActiveItem(1);
    },
    logout: function () {
        this.getViewport().getLayout().setActiveItem(0);
    },
    load_user: function () {
        this.getUser().show();
    }
    /*,
    save_user: function () {
        var userForm = this.getUser().query('.mayavi_user_form')[0];
        var formValues = userForm.getValues();
        console.log("save_user" + JSON.stringify(formValues));
        var userModel = Ext.create('Mayavi.model.User',formValues)
        var store = Ext.data.StoreManager.lookup('User');
        store.add(userModel);
        store.sync({
            success: function(){
                Ext.Msg.alert('Status', 'Changes saved successfully.');
                userForm.getForm().reset();
            },
            failure: function(batch, options) {
                console.log("failure");
                console.log(batch.operations[0]);
                console.log(batch.operations[0].request.action);
                console.log(batch.operations[0].getError());
            }
            //failure: function(){
            //    Ext.Msg.alert('Status', 'Error in saving data.');
            //}
        });

    },*/
});
