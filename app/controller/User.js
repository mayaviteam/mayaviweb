Ext.define('Mayavi.controller.User',{
    extend: 'Ext.app.Controller',
    views: ['user.Form','user.Grid'],
    refs: [
        {
            ref: 'form',
            selector: 'mayavi_user_form'
        },
        {
            ref: 'grid',
            selector: 'mayavi_user_grid'
        }
    ],
    init: function () {
        this.control({
            'button[action=doSaveUser]': {
                click: 'save_user'
            },
            'grid actioncolumn': {
                click: 'edit_delete_user'
            }
        });
		var role_store = Ext.data.StoreManager.lookup('Role');
        role_store.on('load',function(store, records, successful, eOpts ){
            var _columns = [];
            var role_checkboxes = [];
            var _userModelType = Mayavi.model.User;
            var _fields = _userModelType.getFields();
            for (var i = 0; i < records.length; i++) {
                // User form
                var checkbox = {
                    xtype: 'checkboxfield',
                    name: 'roles',
                    boxLabel: records[i].get('name'),
                    inputValue: {'id':records[i].get('id'),'name':records[i].get('name')}
                };
                role_checkboxes.push(checkbox);
                // User Grid
                _columns.push({
                    xtype: 'checkcolumn',
                    text:records[i].get('name'),
                    dataIndex: records[i].get('name')
                });
                // User model
                _fields.push({
                    name:records[i].get('name'),
                    type:'boolean',
                    persist: false,
                    convert: function(v, record){
                        var ret = false;
                        //console.log(JSON.stringify(record.raw.roles));
                        if(record.raw.roles){
                            for(i=0;i<record.raw.roles.length;i++){
                                var role = record.raw.roles[i]
                                if(this.name == role.name){
                                    ret = true;
                                    break;
                                }
                            }
                        }
                        return ret;
                    }
                });
            }

            // Push actin columns after dynamic columns
            var columns = this.getGrid()._columns;
            columns.push({text:'Roles', columns:_columns});
            //columns.push(_columns);
            columns.push(this.getGrid()._action_column);

            // Inject new fields to User Model
            _userModelType.setFields(_fields);

            // Add Cusin check boxes to Form
            var obj = {
                xtype: 'fieldcontainer',
                fieldLabel: 'Roles',
                defaultType: 'checkboxfield',
                items: role_checkboxes
            }
            this.getForm().add(obj);

            //Load user and reconfigure grid with dnamic columns
            var user_store = Ext.data.StoreManager.lookup('User');
            user_store.load({
                scope: this,
                callback: function(){
                    this.getGrid().reconfigure(user_store,columns);
                }
            });
        }, this);

    },
    save_user: function () {
        //var userForm = this.getUser().query('.mayavi_user_form')[0];
        var userForm = this.getForm();
        var formValues = userForm.getValues();
        console.log("save_user" + JSON.stringify(formValues));

        //if roles is not an array fix it,
        if(!Ext.isArray(formValues.roles)){
            if(formValues.roles){
                formValues.roles = [formValues.roles];
            }else{
                formValues.roles = []
            }
        }

        var userModel = Ext.create('Mayavi.model.User',formValues)
        var store = Ext.data.StoreManager.lookup('User');
        store.add(userModel);
        store.sync({
            success: function(){
                Ext.Msg.alert('Status', 'Changes saved successfully.');
                userForm.getForm().reset();		
            },
            failure: function(batch, options) {
                console.log("failure");
            }
        });
    },
    edit_delete_user: function(view, cell, rowIndex, colIndex, e){
        var action = e.target.getAttribute('class');
        if (action.indexOf("x-action-col-0") != -1) {

        } else if (action.indexOf("x-action-col-1") != -1) {
            //TODO Add confirmation
            var store = Ext.data.StoreManager.lookup('User');
            var record = store.getAt(rowIndex);
            store.remove(record);
            store.sync({
                success: function(){
                    Ext.Msg.alert('Status', 'Deleted successfully.');
                },
                failure: function(batch, options) {
                    alert('Error');
                }
            });
        }
    }
});
