Ext.define('Mayavi.store.Role', {
    extend: 'Ext.data.Store',
	model: 'Mayavi.model.Role',
    autoLoad: true
});
